image: ruby:2.3

stages:
 - test
 - staging
 - production

variables:
  OUT_DIR: build
  STAGING_URL: $CI_COMMIT_REF_NAME-$CI_PROJECT_NAME-$CI_PROJECT_NAMESPACE.surge.sh


# Common steps required for each type of "Build" (f-droid.org, GitLab Pages, feature branches)
.setup_for_jekyll: &setup_for_jekyll |
  ruby -v
  apt-get update
  apt-get install -y locales zlib1g-dev gettext po4a linkchecker bundler unzip
  echo "en_US UTF-8" > /etc/locale.gen
  locale-gen en_US.UTF-8
  export LANG=en_US.UTF-8
  export LANGUAGE=en_US:en
  export LC_ALL=en_US.UTF-8
  ./tools/i18n.sh po2md
  bundle install --path vendor


#
# This is a manual task for building in preperation to deploy to
# https://f-droid.org. The intention is for it to be run locally using
# `gitlab-runner` each time a tag is found that is signed by a key in
# the whitelist keyring.  Invoke like so:
#
#  gitlab-runner exec docker f-droid.org --pre-build-script ./prepare-for-deploy.py \
#    --docker-volumes "/root/deploy-whitelist-keyring.gpg:/root/.gnupg/pubring.gpg:ro" \
#    --docker-volumes `pwd`/_site:/builds/output --env DEPLOY_DIR=/builds/output
#
# And when it is finished, you should have a directory in _site/build/
# which includes the entire static site ready to be deployed to
# https://f-droid.org.
#
f-droid.org:
  stage: production
  when: manual
  cache:
    paths: [ vendor/ruby ]

  script:
   - '[ ! -d "$DEPLOY_DIR" ] && echo "DEPLOY_DIR env variable must point to a directory" && exit 1'
   - *setup_for_jekyll
   - ./tools/add-artwork.sh
   - 'echo "url: https://f-droid.org" > userconfig.yml'
   - 'echo "baseurl: \"\"" >> userconfig.yml'
   - echo "Additional Jekyll config used for CI:" && cat userconfig.yml
   - bundle exec jekyll build -d $OUT_DIR --config _config.yml,userconfig.yml --trace
   - ./tools/prepare-multi-lang.sh $OUT_DIR
   - cp -r $OUT_DIR $DEPLOY_DIR/

build:
  stage: test
  except: [ master ]
  cache:
    paths: [ vendor/ruby ]
  artifacts:
    paths: [ $OUT_DIR ]

  script:
   - *setup_for_jekyll
   - 'echo "url: https://$STAGING_URL" > userconfig.yml'
   - 'echo baseurl: /$CI_PROJECT_NAME >> userconfig.yml'
   - echo "Additional Jekyll config used for CI:" && cat userconfig.yml
   - bundle exec jekyll build -d $OUT_DIR/$CI_PROJECT_NAME --config _config.yml,userconfig.yml --trace
   - ./tools/prepare-multi-lang.sh $OUT_DIR/$CI_PROJECT_NAME --no-type-maps
   - ruby -run -e httpd $OUT_DIR -p 4000 2&>1 /dev/null &
   - 'linkchecker http://localhost:4000/$CI_PROJECT_NAME --ignore-url "fdroid.app:.*" --ignore-url ".*/packages/[b-z].*" --ignore-url "/201[0-5]/\d\d/\d\d/" --ignore-url "/news/$"'


# This deploys feature branches to https://surge.sh, a free static site hosting service.
# See CONTRIBUTING.md for details of how to configure your GitLab project to enable this.
# Ideally we would use GitLab pages for this, but until they suupport deploying feature
# branches alongside master branches (https://gitlab.com/gitlab-org/gitlab-pages/issues/33)
# this is the next best option.
#
# Note that although we are building the entire F-Droid repo in the `build` stage, this `preview`
# stage only builds the GP repo. This is because i18n versions of f-droid.org increase the size
# well beyond the 100MiB limit of surge.sh.
preview:
  stage: staging
  except: [ master ]

  # This is allowed to fail because only those who have setup $SURGE_LOGIN and $SURGE_TOKEN
  # as GitLab secret variables (see CONTRIBUTING.md) are able to preview like this.
  allow_failure: true

  script:
   - *setup_for_jekyll
   - 'echo "url: https://$STAGING_URL" > userconfig.yml'
   - 'echo baseurl: /$CI_PROJECT_NAME >> userconfig.yml'
   - 'echo fdroid-repo: https://guardianproject.info/fdroid/repo >> userconfig.yml'
   - echo "Additional Jekyll config used for CI:" && cat userconfig.yml
   - bundle exec jekyll build -d $OUT_DIR/$CI_PROJECT_NAME --config _config.yml,userconfig.yml --trace
   - ./tools/prepare-multi-lang.sh $OUT_DIR/$CI_PROJECT_NAME --no-type-maps
   - apt-get install -y npm
   - npm install -g surge
   - update-alternatives --install /usr/bin/node node /usr/bin/nodejs 10
   - surge --project $OUT_DIR --domain $STAGING_URL

  environment:
    # Should ideally use $STAGING_URL here, but seems custom variables aren't supported.
    url: https://$CI_COMMIT_REF_NAME-$CI_PROJECT_NAME-$CI_PROJECT_NAMESPACE.surge.sh/$CI_PROJECT_NAME
    name: review/$CI_COMMIT_REF_NAME
    on_stop: teardown_preview


teardown_preview:
  stage: staging
  image: node:wheezy
  except: [ master ]
  when: manual
  variables:
    GIT_STRATEGY: none

  # This is allowed to fail because only those who have setup $SURGE_LOGIN and $SURGE_TOKEN
  # as GitLab secret variables (see CONTRIBUTING.md) are able to preview like this.
  allow_failure: true

  script:
   - npm install -g surge
   - surge teardown $STAGING_URL

  environment:
    name: review/$CI_COMMIT_REF_NAME
    action: stop


pages:
  stage: staging
  only: [ master ]
  cache:
    paths: [ vendor/ruby ]
  artifacts:
    paths: [ public ]

  script:
   - *setup_for_jekyll

   # This is where GitLab pages will deploy to by default (e.g. "https://fdroid.gitlab.io/fdroid-website")
   # so we need to make sure that the Jekyll configuration understands this.
   - 'echo url: https://$CI_PROJECT_NAMESPACE.gitlab.io > userconfig.yml'
   - 'echo baseurl: /$CI_PROJECT_NAME >> userconfig.yml'

   - echo "Additional Jekyll config used for CI:" && cat userconfig.yml
   - bundle exec jekyll build -d public --config _config.yml,userconfig.yml
   - ./tools/prepare-multi-lang.sh public --no-type-maps
